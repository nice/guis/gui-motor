# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Verify and fill MACROS accordingly
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# Read information for the devices from an XML file
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import ActionInfos, OpenDisplayActionInfo

# -----------------------------------------------------------------------------
# Verification if the script is being called from Phoebus or from CS-Studio running on Eclipse
# -----------------------------------------------------------------------------
if 'PHOEBUS' in dir(ScriptUtil):
    from org.phoebus.pv import PVFactory
    from org.phoebus.framework.macros import Macros
else:
    from org.csstudio.display.builder.runtime.pv import PVFactory, RuntimePVFactory
    from org.csstudio.display.builder.model.macros import Macros


import xml.etree.ElementTree as ElementTree
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Definition of constants used
# -----------------------------------------------------------------------------
embedded_width  = 117
embedded_height = 199
motors_by_row   = 6
 
# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()

# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def verificationProcedure():
    # Locate XML file relative to the display file
    display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
    directory    = os.path.dirname(display_file)
    display      = widget.getDisplayModel()
    old_macros   = display.getEffectiveMacros()
    motor_name   = old_macros.getValue('motor_name')
    lab_name     = old_macros.getValue('lab_name')
    group_name   = old_macros.getValue('group_name')
    local_config = True
    # Debug
    # logger.info("motor_name: %s" % str(motor_name))
    if motor_name == '' or motor_name == None:
        file = directory + "/scripts/motors.xml"
    else:
        file = directory + "/../../../mainDisplays/%s/%s/scripts/devices.xml" % (lab_name, group_name)
        # if file with version folder does not exists.
        if not os.path.isfile(file):
            file = directory + "/../../../../mainDisplays/%s/%s/scripts/devices.xml" % (lab_name, group_name)
        local_config = False

    # Components settings
   
    x_upCorner      = 10
    y_upCorner      = 25
    motor_number    = 0
    # -------------------------------------------------------------------------
    # Parse XML
    # -------------------------------------------------------------------------
    # Actual content of the XML file would of course depend on what's needed to describe one motor.
    xml = ElementTree.parse(file).getroot()
    root_config = []
    if local_config:
        device = xml
    else:
        # Gets specifically the 'device' element which macro for 'motor_name' is the one we need;
        # should be only one, but if developer configure more, it will take the latest...
        for device in xml.findall("./device/macros/[motor_name='%s']..." % motor_name):
            pass
            #logger.info("device: %s" % str(device))
    for element in device:
        # Debug
        # logger.info("element.tag: %s" % str(element.tag))
        if element.tag in {"motors", "shutters"}:
            root_config = element
            # Process the motor configuration according to specif root which has been read before
            for macros in root_config:
                motor_macros = dict()
                for macro in macros:
                    # Debug purposes...
                    # logger.info("macro.tag: %s; macro.text: %s" % (str(macro.tag), str(macro.text)))
                    motor_macros[macro.tag] = macro.text
                # logger.info("macros: %s" % str(motor_macros))
                # Calculating new X and Y coordinates...
                x = x_upCorner + (motor_number * embedded_width)
                y = y_upCorner
                # Creating a new instance of an Embedded motion window
                display_file = "{}_template.bob".format(element.tag)
                instance = createInstance(x, y, motor_macros, display_file)
                # Adding the created Embedded(motor) to the Display
                display.runtimeChildren().addChild(instance)
                # Updating control variables used to position the buttons on screen
                motor_number += 1
                if motor_number == motors_by_row:
                    # reset count and increase X position (inclue a new column in practice)
                    motor_number = 0
                    x_upCorner = 10
                    y_upCorner += embedded_height + 10
                else:
                    x_upCorner += 10



# -----------------------------------------------------------------------------
# Create display:
# -----------------------------------------------------------------------------
# For each 'device', add one action_button display which then links to the correspondent
# .bob display with the macros of the desired device.
def createInstance(x, y, macros, display_file):
    # Creating an instance of an openDisplayActionInfo using necessary specific objects, and add it to a list of ActionInfos
    # display_file = ('../../../devDisplays/%s/bob/%s.bob' % (settings.get('name'), settings.get('name')))
    # display_file = ('motor_template.bob')
    # Creating an instance of an ActionButton
    embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget()
    embedded.setPropertyValue("x", x)
    embedded.setPropertyValue("y", y)
    embedded.setPropertyValue("width", embedded_width)
    embedded.setPropertyValue("height", embedded_height)
    for macro, value in macros.items():
        embedded.getPropertyValue("macros").add(macro, value)
    embedded.setPropertyValue("file", display_file)
    return embedded


# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
#sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
verificationProcedure()
